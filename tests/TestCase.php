<?php

namespace MyHotelBike\LaravelFormsTests;


use Illuminate\Support\Arr;

class TestCase extends \Orchestra\Testbench\TestCase
{
    protected function getPackageProviders($app)
    {
        return [
            \MyHotelBike\LaravelForms\ServiceProvider::class,
        ];
    }

    public function getOldInput($key, $default) {
        return Arr::get($this->app['session']->get('_old_input', []), $key, $default);
    }
}
