<?php

namespace MyHotelBike\LaravelFormsTests;


use MyHotelBike\LaravelForms\Helpers\Renderable;
use MyHotelBike\LaravelForms\Tags\SelfClosingTag;
use MyHotelBike\LaravelForms\Tags\Tag;

final class SelfClosingTagTest extends TestCase
{
    public function testRenderNoAttributes() {
        $tag = new SelfClosingTag('tag');

        $this->assertEquals('<tag />', $tag->render());
    }

    public function testRenderWithAttributes() {
        $tag = new SelfClosingTag('tag');
        $tag->addAttribute('class', 'container');

        $this->assertEquals('<tag class="container" />', $tag->render());
    }
}
