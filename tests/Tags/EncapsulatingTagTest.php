<?php

namespace MyHotelBike\LaravelFormsTests;


use MyHotelBike\LaravelForms\Helpers\Renderable;
use MyHotelBike\LaravelForms\Tags\EncapsulatingTag;
use MyHotelBike\LaravelForms\Tags\SelfClosingTag;
use MyHotelBike\LaravelForms\Tags\Tag;

final class EncapsulatingTagTest extends TestCase
{
    public function testRenderNoAttributes() {
        $tag = new EncapsulatingTag('tag');

        $this->assertEquals("<tag>\n</tag>", $tag->render());
    }

    public function testRenderWithAttributes() {
        $tag = new EncapsulatingTag('tag');
        $tag->addAttribute('class', 'container');

        $this->assertEquals("<tag class=\"container\">\n</tag>", $tag->render());
    }

    public function testRenderWithChildren() {
        $tag = new EncapsulatingTag('tag');
        $tag->setChild(new SelfClosingTag('br'), 'br');

        $this->assertEquals("<tag>\n<br />\n</tag>", $tag->render());
    }

    public function testRenderWithAttributesWithChildren() {
        $tag = new EncapsulatingTag('tag');
        $tag->addAttribute('class', 'container');
        $tag->setChild(new SelfClosingTag('br'), 'br');

        $this->assertEquals("<tag class=\"container\">\n<br />\n</tag>", $tag->render());
    }
}
