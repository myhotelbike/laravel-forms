<?php

namespace MyHotelBike\LaravelFormsTests;


use MyHotelBike\LaravelForms\Helpers\Renderable;
use MyHotelBike\LaravelForms\Tags\Tag;

final class TestTag extends Tag
{
    public function render(array $parents = []): string
    {
        return '';
    }
}

final class TagTest extends TestCase
{
    public function testAttributesEmpty()
    {
        $tag = new TestTag('tag');

        $this->assertEmpty($tag->getAttributes());
        $this->assertEmpty($tag->getAttribute('class'));
    }

    public function testAttributesAdd()
    {
        $tag = new TestTag('tag');
        $tag->addAttribute('class', 'container');

        $this->assertEquals(['class' => ['container']], $tag->getAttributes());
        $this->assertEquals(['container'], $tag->getAttribute('class'));
    }

    public function testAttributesAddSecond()
    {
        $tag = new TestTag('tag');
        $tag->addAttribute('class', 'container');
        $tag->addAttribute('class', 'row');

        $this->assertEquals(['class' => ['container', 'row']], $tag->getAttributes());
        $this->assertEquals(['container', 'row'], $tag->getAttribute('class'));
    }

    public function testAttributesSet()
    {
        $tag = new TestTag('tag');
        $tag->addAttribute('class', 'container');
        $tag->setAttribute('class', 'row');

        $this->assertEquals(['class' => ['row']], $tag->getAttributes());
        $this->assertEquals(['row'], $tag->getAttribute('class'));
    }

    public function testAttributesClear()
    {
        $tag = new TestTag('tag');
        $tag->addAttribute('class', 'container');
        $tag->clearAttributes();

        $this->assertEmpty($tag->getAttributes());
        $this->assertEmpty($tag->getAttribute('class'));
    }

    public function testAttributesRemove()
    {
        $tag = new TestTag('tag');
        $tag->addAttribute('class', 'container');
        $tag->removeAttribute('class');

        $this->assertEmpty($tag->getAttributes());
        $this->assertEmpty($tag->getAttribute('class'));
    }

    public function testPrefixNameAttribute()
    {
        $tag = new TestTag('tag');

        $this->assertEquals('tag', $tag->prefixNameAttribute('tag', []));
        $this->assertEquals('parent[tag]', $tag->prefixNameAttribute('tag', ['parent']));
        $this->assertEquals('grand[parent][tag]', $tag->prefixNameAttribute('tag', ['grand', 'parent']));
    }

    public function testPrefixIdAttribute()
    {
        $tag = new TestTag('tag');

        $this->assertEquals('tag', $tag->prefixIdAttribute('tag', []));
        $this->assertEquals('parent-tag', $tag->prefixIdAttribute('tag', ['parent']));
        $this->assertEquals('grand-parent-tag', $tag->prefixIdAttribute('tag', ['grand', 'parent']));
    }

    public function testRenderAttributesEmpty()
    {
        $tag = new TestTag('tag');

        $this->assertEquals('', $tag->renderAttributes([]));
        $this->assertEquals('', $tag->renderAttributes(['parent']));
    }

    public function testRenderAttributesSingle()
    {
        $tag = new TestTag('tag');
        $tag->addAttribute('class', 'container');

        $this->assertEquals(' class="container"', $tag->renderAttributes([]));
        $this->assertEquals(' class="container"', $tag->renderAttributes(['parent']));
    }

    public function testRenderAttributesMultipleValues()
    {
        $tag = new TestTag('tag');
        $tag->addAttribute('class', 'container row');

        $this->assertEquals(' class="container row"', $tag->renderAttributes([]));
        $this->assertEquals(' class="container row"', $tag->renderAttributes(['parent']));
    }

    public function testRenderAttributesMultiple()
    {
        $tag = new TestTag('tag');
        $tag->addAttribute('class', 'container');
        $tag->addAttribute('required', 'required');

        $this->assertEquals(' class="container" required="required"', $tag->renderAttributes([]));
        $this->assertEquals(' class="container" required="required"', $tag->renderAttributes(['parent']));
    }

    public function testRenderAttributesName() {
        $tag = new TestTag('tag');
        $tag->addAttribute('name', 'tag');

        $this->assertEquals(' name="tag"', $tag->renderAttributes([]));
        $this->assertEquals(' name="parent[tag]"', $tag->renderAttributes(['parent']));
    }

    public function testRenderAttributesId() {
        $tag = new TestTag('tag');
        $tag->addAttribute('id', 'tag');

        $this->assertEquals(' id="tag"', $tag->renderAttributes([]));
        $this->assertEquals(' id="parent-tag"', $tag->renderAttributes(['parent']));
    }

    public function testRenderAttributesNameMultiple() {
        $tag = new TestTag('tag');
        $tag->addAttribute('name', 'tag');
        $tag->addAttribute('multiple', 'multiple');

        $this->assertEquals(' name="tag[]" multiple="multiple"', $tag->renderAttributes([]));
        $this->assertEquals(' name="parent[tag][]" multiple="multiple"', $tag->renderAttributes(['parent']));
    }
}
