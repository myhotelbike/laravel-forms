<?php

namespace MyHotelBike\LaravelFormsTests;


use MyHotelBike\LaravelForms\Elements\Buttons\Submit;

final class SubmitTest extends TestCase
{
    public function testDefault() {
        $button = new Submit('button', 'Button');
        $expected = <<<END
<button class="btn btn-secondary" type="submit" id="button" title="Button">
Button
</button>
END;
        $this->assertEquals($expected, $button->render());
    }
}
