<?php

namespace MyHotelBike\LaravelFormsTests;


use MyHotelBike\LaravelForms\Elements\Buttons\Button;

final class ButtonTest extends TestCase
{
    public function testDefault() {
        $button = new Button('button', 'Button');
        $expected = <<<END
<button class="btn btn-secondary" type="button" id="button" title="Button">
Button
</button>
END;
        $this->assertEquals($expected, $button->render());
    }
}
