<?php

namespace MyHotelBike\LaravelFormsTests;

use MyHotelBike\LaravelForms\Elements\Fields\Field;
use MyHotelBike\LaravelForms\Tags\EncapsulatingTag;

class TestField extends Field
{
    public function buildTag()
    {
        return new EncapsulatingTag('div');
    }
}

final class FieldTest extends TestCase
{
    public function testDefaultValue()
    {
        $field = new TestField('field');
        $this->assertEquals(null, $field->getDefaultValue());

        $field->setDefaultValue('default');
        $this->assertEquals('default', $field->getDefaultValue());
    }

    public function testSetValues()
    {
        $data = ['field' => 'value'];
        $field = new TestField('field');
        $field->setDefaultValue('default');

        $field->setValues($data);

        $this->assertEquals('value', $field->getDefaultValue());
    }
}
