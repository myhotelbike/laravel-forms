<?php

namespace MyHotelBike\LaravelFormsTests;

use Carbon\Carbon;
use MyHotelBike\LaravelForms\Elements\Fields\DateTime;

final class DateTimeTest extends TestCase
{
    public function testGetValues()
    {
        $element = new DateTime('datetime');

        $values = $element->getValues(['datetime' => ['date' => '2018-02-03', 'time' => '15:15']]);

        $this->assertEquals('2018-02-03 15:15', $values['datetime']);
    }

    public function testSetDefaultValueCarbon() {
        $element = new DateTime('datetime');

        $element->setDefaultValue(new Carbon('2018-02-03 15:15'));

        $this->assertEquals('2018-02-03', $element->getChild('date')->getDefaultValue());
        $this->assertEquals('15:15:00', $element->getChild('time')->getDefaultValue());
    }

    public function testSetDefaultValueDatetime() {
        $element = new DateTime('datetime');

        $element->setDefaultValue(new \DateTime('2018-02-03 15:15'));

        $this->assertEquals('2018-02-03', $element->getChild('date')->getDefaultValue());
        $this->assertEquals('15:15:00', $element->getChild('time')->getDefaultValue());
    }

    public function testSetDefaultValueString() {
        $element = new DateTime('datetime');

        $element->setDefaultValue('2018-02-03 15:15');

        $this->assertEquals('2018-02-03', $element->getChild('date')->getDefaultValue());
        $this->assertEquals('15:15:00', $element->getChild('time')->getDefaultValue());
    }
}
