<?php

namespace MyHotelBike\LaravelFormsTests;

use MyHotelBike\LaravelForms\Elements\Fields\Options;

final class OptionsTest extends TestCase
{
    public function testOptions()
    {
        $select = new Options('test', 'Test');
        $options = ['1' => 'One', '2' => 'Two'];
        $select->setOptions($options);

        $children = [];
        foreach ($select->getChildren() as $child) {
            $children[$child->getId()] = $child->getLabel();
        }

        $this->assertEquals($options, $children);
        $this->assertEquals('in:"1","2"', (string)$select->getRules()[$select->getId()][0]);
    }

    public function testValues()
    {
        $select = new Options('test', 'Test');

        $this->assertEquals(['test' => '1'], $select->getValues(['test' => '1']));
    }
}
