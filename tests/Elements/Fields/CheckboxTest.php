<?php

namespace MyHotelBike\LaravelFormsTests;


use MyHotelBike\LaravelForms\Elements\Fields\Checkbox;

final class CheckboxTest extends TestCase
{
    public function testDefault() {
        $checkbox = new Checkbox('checkbox', 'Checkbox');
        $expected = <<<END
<div class="form-check">
<input class="form-check-input" type="checkbox" value="1" id="checkbox" />
<label class="form-check-label" for="checkbox">
Checkbox
</label>
</div>
END;
        $this->assertEquals($expected, $checkbox->render());
    }
}
