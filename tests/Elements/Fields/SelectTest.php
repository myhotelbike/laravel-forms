<?php

namespace MyHotelBike\LaravelFormsTests;


use MyHotelBike\LaravelForms\Elements\Fields\Select;

final class SelectTest extends TestCase
{
    public function testOptions()
    {
        $select = new Select('test', 'Test');
        $options = ['1' => 'One', '2' => 'Two'];
        $select->setOptions($options);

        $children = [];
        foreach ($select->getChildren() as $child) {
            $children[$child->getAttribute('value')[0]] = $child->getChild('text')->getText();
        }

        $this->assertEquals(['' => 'Select a test'] + $options, $children);
        $this->assertEquals('in:"1","2"', (string)$select->getRules()[$select->getId()][0]);
    }

    public function testValues()
    {
        $select = new Select('test', 'Test');

        $this->assertEquals(['test' => '1'], $select->getValues(['test' => '1']));
    }
}
