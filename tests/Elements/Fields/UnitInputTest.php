<?php

namespace MyHotelBike\LaravelFormsTests;

use MyHotelBike\LaravelForms\Elements\Fields\UnitInput;

final class UnitInputTest extends TestCase
{
    public function testToValueUnit()
    {
        $input = new UnitInput('input', 'Input');
        $input->setUnits([1 => 'second', 60 => 'minute']);

        $this->assertEquals([1, 1], $input->toValueUnit(1));
        $this->assertEquals([10, 1], $input->toValueUnit(10));
        $this->assertEquals([1, 60], $input->toValueUnit(60));
        $this->assertEquals([61, 1], $input->toValueUnit(61));
        $this->assertEquals([2, 60], $input->toValueUnit(120));
    }

    public function testToSingle()
    {
        $input = new UnitInput('input', 'Input');

        $this->assertEquals(1, $input->toSingle(1, 1));
        $this->assertEquals(10, $input->toSingle(1, 10));
        $this->assertEquals(60, $input->toSingle(1, 60));
    }

    public function testGetValues()
    {
        $input = new UnitInput('input', 'Input');
        $input->setUnits([1 => 'second', 60 => 'minute']);

        $this->assertEquals(['input' => 1], $input->getValues(['input' => ['value' => 1, 'unit' => 1]]));
        $this->assertEquals(['input' => 10], $input->getValues(['input' => ['value' => 10, 'unit' => 1]]));
        $this->assertEquals(['input' => 60], $input->getValues(['input' => ['value' => 1, 'unit' => 60]]));
        $this->assertEquals(['input' => 120], $input->getValues(['input' => ['value' => 2, 'unit' => 60]]));
    }
}
