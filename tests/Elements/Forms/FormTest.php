<?php

namespace MyHotelBike\LaravelFormsTests;

use Illuminate\Validation\ValidationException;
use MyHotelBike\LaravelForms\Elements\Fields\Input;
use MyHotelBike\LaravelForms\Elements\Forms\Form;


final class FormTest extends TestCase
{
    public function testGetId()
    {
        $form = new Form();

        $this->assertEquals('form', $form->getId());
    }

    public function testAction()
    {
        $form = new Form();
        $this->assertEmpty($form->getAttribute('action'));

        $form->setAction('/');
        $this->assertEquals(['/'], $form->getAttribute('action'));
    }

    public function testMethod()
    {
        $form = new Form();
        $this->assertEquals(['POST'], $form->getAttribute('method'));

        $form->setMethod('get');
        $this->assertEquals(['GET'], $form->getAttribute('method'));
    }

    public function testValues()
    {
        $form = new Form();
        $form->setChild(new Input('test', 'Test'), 'test');

        $this->assertEquals(['test' => '1'], $form->getValues(['form' => ['test' => '1', 'not' => 'not']]));
    }

    public function testValidate() {
        $form = new Form();
        $input = new Input('input', 'Input');
        $input->addRule('required');
        $form->setChild($input, 'input');

        $this->assertEquals(['form.input' => ['required']], $form->getRules());

        $values = $form->validate(['form' => ['input' => 1]]);

        $this->assertEquals(['input' => 1], $values);

        $this->expectException(ValidationException::class);
        $form->validate([]);
    }
}
