<?php

namespace MyHotelBike\LaravelFormsTests;


use MyHotelBike\LaravelForms\Elements\Element;
use MyHotelBike\LaravelForms\Tags\EncapsulatingTag;
use MyHotelBike\LaravelForms\Tags\Tag;

final class TestElement extends Element {
    public function buildTag()
    {
        return new EncapsulatingTag('div');
    }
}

final class ElementTest extends TestCase
{
    public function testId()
    {
        $element = new TestElement('element');

        $this->assertEquals('element', $element->getId());

        $element->setId('Other id');

        $this->assertEquals('other_id', $element->getId());
    }

    public function testLabel()
    {
        $element = new TestElement('element', 'Label');

        $this->assertEquals('Label', $element->getLabel());

        $element->setLabel('Other label');

        $this->assertEquals('Other label', $element->getLabel());
    }

    public function testIsParent()
    {
        $element = new TestElement('element');

        $this->assertEquals(false, $element->getIsParent());

        $element->setIsParent();

        $this->assertEquals(true, $element->getIsParent());
    }

    public function testRulesNoParentNoChildren()
    {
        $element = new TestElement('element');
        $element->addRule('required');

        $this->assertEquals(['element' => ['required']], $element->getRules());
    }

    public function testRulesNoParentWithChildren() {
        $element = new TestElement('element');
        $element->addRule('required');
        $child = new TestElement('child');
        $child->addRule('unique');
        $element->setChild($child, 'child');

        $this->assertEquals(['element' => ['required'], 'child' => ['unique']], $element->getRules());
    }

    public function testRulesIsParentNoChildren() {
        $element = new TestElement('element');
        $element->addRule('required');

        $this->assertEquals(['element' => ['required']], $element->getRules());
    }

    public function testRulesIsParentWithChildren() {
        $element = new TestElement('element');
        $element->addRule('required');
        $element->setIsParent();
        $child = new TestElement('child');
        $child->addRule('unique');
        $element->setChild($child, 'child');

        $this->assertEquals(['element' => ['required'], 'element.child' => ['unique']], $element->getRules());
    }

    public function testChildren() {
        $element = new TestElement('element');
        $child = new TestElement('child');
        $this->assertEmpty($element->getChildren());

        $element->setChild($child, 'child');
        $this->assertCount(1, $element->getChildren());
        $this->assertEquals($child, $element->getChild('child'));

        $element->removeChild('child');
        $this->assertEmpty($element->getChildren());
    }

    public function testValuesNoParentNoChildren() {
        $element = new TestElement('element');

        $this->assertEmpty($element->getValues([]));
        $this->assertEmpty($element->getValues(['child' => 'value']));
        $this->assertEquals(['element' => 'value'], $element->getValues(['element' => 'value']));
    }

    public function testValuesNoParentWithChildren() {
        $element = new TestElement('element');
        $child = new TestElement('child');
        $element->setChild($child, 'child');

        $this->assertEquals(['element' => 'value'], $element->getValues(['element' => 'value']));
        $this->assertEquals(['child' => 'value2'], $element->getValues(['child' => 'value2']));
        $this->assertEquals(['element' => 'value', 'child' => 'value2'], $element->getValues(['element' => 'value', 'child' => 'value2']));
    }

    public function testValuesIsParentWithChildren() {
        $element = new TestElement('element');
        $element->setIsParent();
        $child = new TestElement('child');
        $element->setChild($child, 'child');

        $this->assertEmpty($element->getValues([]));
        $this->assertEmpty($element->getValues(['element' => []]));
        $this->assertEquals(['element' => ['child' => 'value']], $element->getValues(['element' => ['child' => 'value']]));
    }

    public function testRenderNoParent() {
        $tag = \Mockery::mock(Tag::class);
        $tag->shouldReceive('render')->withArgs([[]])->once()->andReturn('Text');
        $element = new TestElement('element');
        $element->setTag($tag);

        $this->assertEquals('Text', $element->render());
    }

    public function testRenderIsParent() {
        $tag = \Mockery::mock(Tag::class);
        $tag->shouldReceive('render')->withArgs([['element']])->once()->andReturn('Text');
        $element = new TestElement('element');
        $element->setIsParent();
        $element->setTag($tag);

        $this->assertEquals('Text', $element->render());
    }

    public function testRenderWithParents() {
        $tag = \Mockery::mock(Tag::class);
        $tag->shouldReceive('render')->withArgs([['parent', 'element']])->once()->andReturn('Text');
        $element = new TestElement('element');
        $element->setIsParent();
        $element->setTag($tag);

        $this->assertEquals('Text', $element->render(['parent']));
    }
}
