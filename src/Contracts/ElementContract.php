<?php

namespace MyHotelBike\LaravelForms\Contracts;

interface ElementContract
{
    public function getId(): string;

    public function setId(string $id);

    public function getLabel(): string;

    public function setLabel(string $label);

    public function getRules(): array;

    public function addRule($rule);

    public function getValues(array $data): array;
}
