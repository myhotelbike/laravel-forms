<?php

namespace MyHotelBike\LaravelForms\Helpers;


class Text implements Renderable
{
    /** @var string */
    private $text;

    public function __construct(string $text)
    {
        $this->text = $text;
    }

    public function getText() {
        return $this->text;
    }

    public function render(array $parents = []): string
    {
        return $this->text;
    }
}
