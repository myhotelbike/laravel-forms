<?php

namespace MyHotelBike\LaravelForms\Helpers;


interface Renderable
{
    public function render(array $parents = []): string;
}
