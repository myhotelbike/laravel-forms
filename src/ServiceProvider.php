<?php

namespace MyHotelBike\LaravelForms;


use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use MyHotelBike\LaravelForms\Fields\Field;
use MyHotelBike\LaravelForms\Wrappers\Bootstrap4FormGroup;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $view_path = __DIR__ . '/../views';
        $this->loadViewsFrom($view_path, 'forms');
        $this->publishes([$view_path => resource_path('views/vendor/forms')]);

        Blade::directive('form', function($expression) {
            return "<?php echo ($expression)->render(); ?>";
        });
    }
}
