<?php

namespace MyHotelBike\LaravelForms\Tags;


use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use MyHotelBike\LaravelForms\Helpers\Renderable;

abstract class Tag implements Renderable
{
    /** @var string */
    private $tag;

    /** @var bool */
    private $root = false;

    /** @var array[string]string[] */
    private $attributes = [];

    public function __construct(string $tag)
    {
        $this->tag = $tag;
    }

    public function getTag()
    {
        return $this->tag;
    }

    public function getRoot()
    {
        return $this->root;
    }

    public function setRoot($root = true)
    {
        $this->root = $root;
    }

    public function setAttribute($key, $values)
    {
        $this->attributes[$key] = Arr::wrap($values);

        return $this;
    }

    public function getAttribute($key, $default = null)
    {
        return $this->attributes[$key] ?? $default;
    }

    public function addAttribute($key, $values)
    {
        $this->attributes[$key] = array_merge($this->attributes[$key] ?? [], Arr::wrap($values));

        return $this;
    }

    public function clearAttributes()
    {
        $this->attributes = [];

        return $this;
    }

    public function removeAttribute($key, $values = null)
    {
        if ($values === null) {
            unset($this->attributes[$key]);
        } else {
            $this->attributes[$key] = array_diff($this->attributes[$key] ?? [], Arr::wrap($values));
        }

        return $this;
    }

    public function getAttributes()
    {
        return $this->attributes;
    }

    public function getChildren()
    {
        return [];
    }

    public function render(array $parents = []): string {
        $html = $this->renderOpening($parents) . PHP_EOL;
        $html .= $this->renderChildren($parents);
        $html .= $this->renderClosing();

        return $html;
    }

    public abstract function renderOpening(array $parents) : string;
    public abstract function renderClosing() : string;

    public function renderChildren(array $parents) : string {
        $html = '';

        foreach ($this->getChildren() as $child) {
            $html .= $child->render($parents) . PHP_EOL;
        }

        return $html;
    }

    public function renderAttributes(array $parents): string
    {
        $attributes = array_filter($this->attributes);

        if (!$this->root && !empty($parents)) {
            foreach ($attributes['name'] ?? [] as $i => $name) {
                $attributes['name'][$i] = $this->prefixNameAttribute($name, $parents);
            }

            foreach ($attributes['id'] ?? [] as $i => $id) {
                $attributes['id'][$i] = $this->prefixIdAttribute($id, $parents);
            }

            foreach ($attributes['for'] ?? [] as $i => $id) {
                $attributes['for'][$i] = $this->prefixIdAttribute($id, $parents);
            }
        }

        $html = '';
        foreach ($attributes as $key => $values) {
            $html .= ' ' . $key . '="' . implode(' ', $values) . '"';
        }
        return $html;
    }

    public function prefixNameAttribute($name, array $parents)
    {
        $multiple = false;
        if (substr($name, -2) == '[]') {
            $multiple = true;
            $name = substr($name, 0, -2);
        }

        $parents[] = $name;
        $name = $parents[0];

        foreach (array_slice($parents, 1) as $parent) {
            $name .= '[' . $parent . ']';
        }

        if ($multiple) {
            $name .= '[]';
        }

        return $name;
    }

    public function prefixIdAttribute($id, array $parents)
    {
        return implode('-', array_map(function ($item) {
            return Str::slug($item, '_');
        }, array_merge($parents, [$id])));
    }

    public abstract function setText(string $text);
}
