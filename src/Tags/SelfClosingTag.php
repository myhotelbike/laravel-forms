<?php

namespace MyHotelBike\LaravelForms\Tags;


class SelfClosingTag extends Tag
{
    public function renderOpening(array $parents = []): string
    {
        return '<' . $this->getTag() . $this->renderAttributes($parents) . ' />';
    }

    public function renderClosing(): string
    {
        return '';
    }

    public function setText(string $text)
    {
        $this->setAttribute('title', $text);
    }
}
