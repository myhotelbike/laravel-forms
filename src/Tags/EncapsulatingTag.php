<?php

namespace MyHotelBike\LaravelForms\Tags;


use MyHotelBike\LaravelForms\Helpers\Renderable;
use MyHotelBike\LaravelForms\Helpers\Text;

class EncapsulatingTag extends Tag
{
    /** @var Renderable[string] */
    private $children = [];

    public function setChild(Renderable $child, string $id)
    {
        $this->children[$id] = $child;
    }

    public function removeChild(string $id)
    {
        unset($this->children[$id]);
    }

    public function clearChildren()
    {
        $this->children = [];
    }

    public function getChild(string $id, $default = null)
    {
        return $this->children[$id] ?? $default;
    }

    public function getChildren()
    {
        return $this->children;
    }

    public function setText(string $text)
    {
        $this->setChild(new Text($text), 'text');
    }

    public function renderOpening(array $parents): string
    {
        return '<' . $this->getTag() . $this->renderAttributes($parents) . '>';
    }

    public function renderClosing(): string
    {
        return '</' . $this->getTag() . '>';
    }
}
