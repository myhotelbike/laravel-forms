<?php

namespace MyHotelBike\LaravelForms\Tags;

class TagGroup extends EncapsulatingTag
{
    public function __construct()
    {
        parent::__construct('');
    }

    public function renderOpening(array $parents): string
    {
        return '';
    }

    public function renderClosing(): string
    {
        return '';
    }

    public function setAttribute($key, $values)
    {
        foreach ($this->getChildren() as $child) {
            $child->setAttribute($key, $values);
        }
    }

    public function getAttribute($key, $default = null)
    {
        $children = $this->getChildren();
        $child = reset($children);

        if (!$child) {
            return $default;
        }

        return $child->getAttribute($key, $default);
    }

    public function addAttribute($key, $values)
    {
        foreach ($this->getChildren() as $child) {
            $child->addAttribute($key, $values);
        }
    }

    public function clearAttributes()
    {
        foreach ($this->getChildren() as $child) {
            $child->clearAttributes();
        }
    }

    public function removeAttribute($key, $values = null)
    {
        foreach ($this->getChildren() as $child) {
            $child->removeAttribute($key, $values);
        }
    }

    public function getAttributes()
    {
        $children = $this->getChildren();
        $child = reset($children);

        if (!$child) {
            return [];
        }

        return $child->getAttributes();
    }
}
