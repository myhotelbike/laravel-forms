<?php

namespace MyHotelBike\LaravelForms\Elements\Forms;

use Illuminate\Support\Str;
use \Illuminate\Support\Facades\Validator;
use MyHotelBike\LaravelForms\Elements\Element;
use MyHotelBike\LaravelForms\Tags\EncapsulatingTag;
use MyHotelBike\LaravelForms\Tags\SelfClosingTag;
use MyHotelBike\LaravelForms\Tags\Tag;

class Form extends Element
{
    public function __construct($action = null, $method = 'POST')
    {
        $id = Str::kebab(class_basename($this));

        parent::__construct($id);

        $this->setIsParent();
        $this->setAction($action);
        $this->setMethod($method);

        $this->build();
    }

    public function buildTag(): Tag
    {
        $form = new EncapsulatingTag('form');
        $form->addAttribute('class', 'form-horizontal');

        return $form;
    }

    public function setAction(?string $action)
    {
        $this->setAttribute('action', $action);
    }

    public function setMethod(string $method)
    {
        $method = Str::upper($method);

        if ($method == 'GET') {
            $this->setAttribute('method', 'GET');
            $this->removeChild('_csrf');
            $this->removeChild('_method');

            return;
        }

        $this->setAttribute('method', 'POST');
        $csrf = new SelfClosingTag('input');
        $csrf->setRoot();
        $csrf->setAttribute('type', 'hidden');
        $csrf->setAttribute('name', '_token');
        $csrf->setAttribute('value', csrf_token());
        $this->setChild($csrf, '_csrf');

        if ($method == 'POST') {
            $this->removeChild('_method');

            return;
        }

        $method_field = new SelfClosingTag('input');
        $method_field->setRoot();
        $method_field->setAttribute('type', 'hidden');
        $method_field->setAttribute('name', '_method');
        $method_field->setAttribute('value', $method);
        $this->setChild($method_field, '_method');

    }

    public function build()
    {

    }

    public function getValues(array $data): array
    {
        $values = parent::getValues($data);

        return $values[$this->getId()] ?? [];
    }

    public function validate(array $data, array $messages = [], array $customAttributes = []): array
    {
        Validator::make($data, $this->getRules(), $messages, $customAttributes + $this->getLabels())
            ->validate();

        return $this->getValues($data);
    }
}
