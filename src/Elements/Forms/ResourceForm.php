<?php

namespace MyHotelBike\LaravelForms\Elements\Forms;

use Illuminate\Database\Eloquent\Model;

abstract class ResourceForm extends Form
{
    protected $model;

    public function __construct(Model $model, $action = null, $method = 'POST')
    {
        $this->model = $model;

        parent::__construct($action, $method);
    }

    public function getModel(): Model
    {
        return $this->model;
    }

    public function save(array $values)
    {
        $this->model->fill($values)->save();
    }
}
