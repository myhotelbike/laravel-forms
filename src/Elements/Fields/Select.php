<?php

namespace MyHotelBike\LaravelForms\Elements\Fields;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use MyHotelBike\LaravelForms\Tags\EncapsulatingTag;
use MyHotelBike\LaravelForms\Tags\Tag;

class Select extends Optgroup
{
    private $options;

    public function buildTag(): Tag
    {
        $tag = new EncapsulatingTag('select');
        $tag->addAttribute('class', 'form-control');

        return $tag;
    }

    public function setMultiple(bool $multiple = true)
    {
        parent::setMultiple($multiple);

        $this->addAttribute('multiple', 'multiple');
    }

    public function setOptions(array $options)
    {
        $this->clearChildren();

        foreach ($options as $key => $value) {
            $this->addOption($key, $value);
        }
    }

    public function clearChildren()
    {
        parent::clearChildren();

        $this->options = [];
        parent::addOption('', 'Select a ' . Str::lower($this->getLabel()));
    }

    public function addOption($key, $value)
    {
        if (is_array($value)) {
            $group = $this->getChild($key) ?? $this->addGroup($key);
            foreach ($value as $_key => $_value) {
                $this->options[$_key] = $_value;
                $group->addOption($_key, $_value);
            }
        } else {
            $this->options[$key] = $value;
            parent::addOption($key, $value);
        }
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function addGroup($label)
    {
        $group = new Optgroup($label, $label);

        $this->setChild($group, $label);

        return $group;
    }

    public function getRules(): array
    {
        $this->addRule(Rule::in(array_keys($this->options)), 'in');

        return parent::getRules();
    }

    public function render(array $parents = []): string
    {
        foreach (Arr::wrap($this->getDefaultValue()) as $key) {
            $option = $this->getChild($key);

            if (!$option) {
                continue;
            }

            $option->setAttribute('selected', 'selected');
        }

        return parent::render($parents);
    }
}
