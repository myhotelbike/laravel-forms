<?php

namespace MyHotelBike\LaravelForms\Elements\Fields;

use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;
use MyHotelBike\LaravelForms\Tags\Tag;
use MyHotelBike\LaravelForms\Tags\TagGroup;

class Options extends Field
{
    private $options;

    public function buildTag(): Tag
    {
        return new TagGroup();
    }

    public function setOptions(array $options)
    {
        $this->clearChildren();

        foreach ($options as $key => $value) {
            $this->addOption($key, $value);
        }
    }

    public function clearChildren()
    {
        parent::clearChildren();

        $this->options = [];
    }

    public function addOption($key, $value)
    {
        $option = new Checkbox($key, $value);
        $this->setChild($option, $key);
        $option->setAttribute('name', $this->getId());
        $option->setAttribute('value', $key);

        $this->options[$key] = $value;
    }

    public function setDefaultValue($data)
    {
        $data = Arr::wrap($data);
        foreach ($this->getChildren() as $child) {
            $child->setDefaultValue(in_array($child->getId(), $data));
        }
    }

    public function setTagMultiple(bool $multiple)
    {
        parent::setTagMultiple($multiple);

        $this->setAttribute('type', $multiple ? 'checkbox' : ' radio');
    }

    public function getRules(): array
    {
        $this->addRule(Rule::in(array_keys($this->options)), 'in');

        return parent::getRules();
    }
}
