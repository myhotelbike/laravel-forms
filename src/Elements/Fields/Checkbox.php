<?php

namespace MyHotelBike\LaravelForms\Elements\Fields;

use MyHotelBike\LaravelForms\Helpers\Text;
use MyHotelBike\LaravelForms\Tags\EncapsulatingTag;
use MyHotelBike\LaravelForms\Tags\SelfClosingTag;
use MyHotelBike\LaravelForms\Tags\Tag;

class Checkbox extends Field
{
    public function buildTag(): Tag
    {
        $wrapper = new EncapsulatingTag('div');
        $wrapper->addAttribute('class', 'form-check');

        $checkbox = new SelfClosingTag('input');
        $checkbox->addAttribute('class', 'form-check-input');
        $checkbox->setAttribute('type', 'checkbox');
        $checkbox->setAttribute('value', '1');
        $wrapper->setChild($checkbox, 'main');

        $label = new EncapsulatingTag('label');
        $label->addAttribute('class', 'form-check-label');
        $wrapper->setChild($label, 'label');

        return $wrapper;
    }

    public function setTagId(string $id)
    {
        $this->getChild('main')->setAttribute('id', $id);
        $this->getChild('label')->setAttribute('for', $id);

        $this->getChild('main')->setAttribute('name', $id);
    }

    public function setTagLabel(string $label)
    {
        $this->getChild('label')->setChild(new Text($label), 'text');
    }

    public function render(array $parents = []): string
    {
        if ($this->getDefaultValue()) {
            $this->getChild('main')->setAttribute('checked', 'checked');
        }

        return parent::render($parents);
    }

    public function setAttribute($key, $values)
    {
        $this->getChild('main')->setAttribute($key, $values);
    }

    public function getAttribute($key, $default = null)
    {
        return $this->getChild('main')->getAttribute($key, $default);
    }

    public function addAttribute($key, $values)
    {
        $this->getChild('main')->addAttribute($key, $values);
    }

    public function clearAttributes()
    {
        $this->getChild('main')->clearAttributes();
    }

    public function removeAttribute($key, $values = null)
    {
        $this->getChild('main')->removeAttribute($key, $values);
    }

    public function getAttributes()
    {
        return $this->getChild('main')->getAttributes();
    }
}
