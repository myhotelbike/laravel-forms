<?php

namespace MyHotelBike\LaravelForms\Elements\Fields;

use MyHotelBike\LaravelForms\Tags\SelfClosingTag;
use MyHotelBike\LaravelForms\Tags\Tag;

class Hidden extends Field
{
    public function buildTag(): Tag
    {
        $tag = new SelfClosingTag('input');
        $tag->setAttribute('type', 'hidden');

        return $tag;
    }
}
