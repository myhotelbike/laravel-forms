<?php

namespace MyHotelBike\LaravelForms\Elements\Fields;

use MyHotelBike\LaravelForms\Tags\SelfClosingTag;
use MyHotelBike\LaravelForms\Tags\Tag;

class Input extends Field
{

    public function buildTag(): Tag
    {
        $tag = new SelfClosingTag('input');
        $tag->addAttribute('class', 'form-control');

        return $tag;
    }

    public function setType($type)
    {
        $this->setAttribute('type', $type);
    }

    public function getType()
    {
        return $this->getAttribute('type');
    }

    public function render(array $parents = []): string
    {
        if ($value = $this->getDefaultValue()) {
            $this->setAttribute('value', $value);
        }

        return parent::render($parents);
    }
}
