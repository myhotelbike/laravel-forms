<?php

namespace MyHotelBike\LaravelForms\Elements\Fields;

use MyHotelBike\LaravelForms\Tags\Tag;
use MyHotelBike\LaravelForms\Tags\TagGroup;

class UnitInput extends Field
{
    private $units = [];

    public function buildTag(): Tag
    {
        $this->setIsParent();

        $tag = new TagGroup();

        $value = new Input('value');
        $value->setType('numeric');
        $tag->setChild($value, 'value');

        $unit = new Select('unit');
        $tag->setChild($unit, 'unit');

        return $tag;
    }

    public function setUnits(array $units)
    {
        $this->units = $units;

        $this->getChild('unit')->setOptions($units);
    }

    public function setTagLabel(string $label)
    {
        $this->getChild('value')->setLabel($label);
        $this->getChild('unit')->setLabel($label);
    }

    public function setDefaultValue($value)
    {
        parent::setDefaultValue($value);

        list($value, $unit) = $this->toValueUnit($value);

        $this->getChild('value')->setDefaultValue($value);
        $this->getChild('unit')->setDefaultValue($unit);
    }

    public function toValueUnit($single)
    {
        if ($single === NULL)
        {
            return [NULL, NULL];
        }

        foreach (array_reverse($this->units, TRUE) as $value_per => $unit)
        {
            if ($single % $value_per == 0) {
                return [$single / $value_per, $value_per];
            }
        }

        return [$single, 1];
    }

    public function getValues(array $data) : array
    {
        $values = parent::getValues($data);

        if (isset($values[$this->getId()])) {
            $values[$this->getId()] = $this->toSingle(
                $values[$this->getId()]['value'] ?? 0,
                $values[$this->getId()]['unit'] ?? 1
            );
        }

        return $values;
    }

    public function toSingle($value, $unit)
    {
        return $value * $unit;
    }
}
