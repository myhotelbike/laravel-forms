<?php

namespace MyHotelBike\LaravelForms\Elements\Fields;

use MyHotelBike\LaravelForms\Helpers\Text;
use MyHotelBike\LaravelForms\Tags\EncapsulatingTag;
use MyHotelBike\LaravelForms\Tags\Tag;

class Textarea extends Field
{
    protected $view = 'forms::fields.textarea';

    public function buildTag(): Tag
    {
        $tag = new EncapsulatingTag('textarea');
        $tag->addAttribute('class', 'form-control');

        return $tag;
    }

    public function setDefaultValue($value)
    {
        parent::setDefaultValue($value);

        $this->setChild(new Text($value), 'value');
    }
}
