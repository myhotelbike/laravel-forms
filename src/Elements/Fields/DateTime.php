<?php

namespace MyHotelBike\LaravelForms\Elements\Fields;

use Carbon\Carbon;
use MyHotelBike\LaravelForms\Tags\Tag;
use MyHotelBike\LaravelForms\Tags\TagGroup;

class DateTime extends Field
{
    public function buildTag(): Tag
    {
        $this->setIsParent();

        $tag = new TagGroup();

        $value = new Input('date');
        $value->setType('date');
        $tag->setChild($value, 'date');

        $value = new Input('time');
        $value->setType('time');
        $tag->setChild($value, 'time');

        return $tag;
    }

    public function setTagLabel(string $label)
    {
        $this->getChild('date')->setLabel($label);
        $this->getChild('time')->setLabel($label);
    }

    public function setDefaultValue($value)
    {
        $value = static::wrap($value);

        parent::setDefaultValue($value);

        $this->getChild('date')->setDefaultValue($value->toDateString());
        $this->getChild('time')->setDefaultValue($value->toTimeString());
    }

    public function getValues(array $data) : array
    {
        $values = parent::getValues($data);

        if (isset($values[$this->getId()])) {
            $values[$this->getId()] = $values[$this->getId()]['date'] . ' ' . $values[$this->getId()]['time'];
        }

        return $values;
    }

    public static function wrap($datetime): Carbon {
        if ($datetime === NULL) {
            return NULL;
        }

        if ($datetime instanceof Carbon)
        {
            return $datetime;
        }
        elseif ($datetime instanceof \DateTime)
        {
            return Carbon::instance($datetime);
        }
        return new Carbon($datetime);
    }
}
