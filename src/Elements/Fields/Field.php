<?php

namespace MyHotelBike\LaravelForms\Elements\Fields;

use MyHotelBike\LaravelForms\Elements\Element;

abstract class Field extends Element
{
    private $defaultValue;

    private $multiple = false;

    public function setDefaultValue($value)
    {
        $this->defaultValue = $value;
    }

    public function getDefaultValue()
    {
        return $this->defaultValue;
    }

    public function setMultiple(bool $multiple = true)
    {
        $this->multiple = $multiple;

        $this->setTagMultiple($multiple);
    }

    public function setTagMultiple(bool $multiple)
    {
        $names = $this->getAttribute('name');

        foreach ($names as $i => $name) {
            $name_multiple = substr($name, -2) == '[]';

            if ($multiple == $name_multiple) {
                continue;
            }

            if ($multiple) {
                $names[$i] = $name . '[]';
            }
            else {
                $names[$i] = substr($name, 0, -2);
            }
        }

        $this->setAttribute('name', $names);
    }

    public function getMultiple()
    {
        return $this->multiple;
    }

    public function setValues(array $data)
    {
        if (isset($data[$this->getId()])) {
            $this->setDefaultValue($data[$this->getId()]);
        }

        parent::setValues($data);
    }

    public function setTagId(string $id)
    {
        parent::setTagId($id);

        $this->setAttribute('name', $this->getId());
    }

    public function getOwnValues($data): array
    {
        return [$this->getId() => $data[$this->getId()] ?? $this->getDefaultValue()];
    }

    public function render(array $parents = []): string
    {
        if (in_array('required', $this->getRules())) {
            $this->setAttribute('required', 'required');
        }

        return parent::render($parents);
    }
}
