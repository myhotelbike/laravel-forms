<?php

namespace MyHotelBike\LaravelForms\Elements\Fields;

use MyHotelBike\LaravelForms\Helpers\Text;
use MyHotelBike\LaravelForms\Tags\EncapsulatingTag;
use MyHotelBike\LaravelForms\Tags\Tag;

class Optgroup extends Field
{
    public function buildTag(): Tag
    {
        $tag = new EncapsulatingTag('optgroup');

        return $tag;
    }

    public function addOption($key, $value) {
        $option = new EncapsulatingTag('option');
        $option->setAttribute('value', $key);
        $option->setChild(new Text($value), 'text');

        $this->setChild($option, $key);

        return $option;
    }
}
