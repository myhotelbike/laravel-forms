<?php

namespace MyHotelBike\LaravelForms\Elements\Wrappers;

use MyHotelBike\LaravelForms\Tags\EncapsulatingTag;
use MyHotelBike\LaravelForms\Tags\Tag;

class Bootstrap4EmptyFormGroup extends Wrapper
{
    public function buildTag(): Tag
    {
        $tag = new EncapsulatingTag('div');
        $tag->addAttribute('class', ['form-group', 'row']);

        $child = new EncapsulatingTag('div');
        $child->addAttribute('class', ['col-sm-8', 'offset-sm-4']);
        $tag->setChild($child, 'child');

        return $tag;
    }

    public function getDefaultParent() {
        return $this->getTag()->getChild('child');
    }
}
