<?php

namespace MyHotelBike\LaravelForms\Elements\Wrappers;

use MyHotelBike\LaravelForms\Helpers\Text;
use MyHotelBike\LaravelForms\Tags\EncapsulatingTag;
use MyHotelBike\LaravelForms\Tags\Tag;

class Bootstrap4InputGroup extends Wrapper
{

    public function buildTag(): Tag
    {
        $tag = new EncapsulatingTag('div');
        $tag->addAttribute('class', 'input-group');

        return $tag;
    }

    public function setPrefix(string $text) {
        $tag = new EncapsulatingTag('div');
        $tag->addAttribute('class', 'input-group-prepend');
        $this->setChild($tag, 'prefix');

        $span = new EncapsulatingTag('span');
        $span->addAttribute('class', 'input-group-text');
        $span->setChild(new Text($text), 'text');
        $tag->setChild($span, 'span');
    }

    public function setSuffix(string $text) {
        $tag = new EncapsulatingTag('div');
        $tag->addAttribute('class', 'input-group-append');
        $this->setChild($tag, 'suffix');

        $span = new EncapsulatingTag('span');
        $span->addAttribute('class', 'input-group-text');
        $span->setChild(new Text($text), 'text');
        $tag->setChild($span, 'span');
    }

    public function setError(string $text) {
        $tag = new EncapsulatingTag('div');
        $tag->addAttribute('class', 'invalid-feedback');
        $tag->setChild(new Text($text), 'text');
        $this->setChild($tag, 'error');
    }
}
