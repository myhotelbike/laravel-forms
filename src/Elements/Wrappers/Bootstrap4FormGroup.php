<?php

namespace MyHotelBike\LaravelForms\Elements\Wrappers;

use MyHotelBike\LaravelForms\Helpers\Text;
use MyHotelBike\LaravelForms\Tags\EncapsulatingTag;
use MyHotelBike\LaravelForms\Tags\Tag;

class Bootstrap4FormGroup extends Wrapper
{
    public function buildTag(): Tag
    {
        $tag = new EncapsulatingTag('div');
        $tag->addAttribute('class', ['form-group', 'row']);

        $label = new EncapsulatingTag('label');
        $label->addAttribute('class', ['col-sm-4 col-form-label']);
        $tag->setChild($label, 'label');

        $child = new EncapsulatingTag('div');
        $child->addAttribute('class', ['col-sm-8']);
        $tag->setChild($child, 'child');

        $group = new Bootstrap4InputGroup('group', '');
        $child->setChild($group, 'group');

        return $tag;
    }

    public function getDefaultParent() {
        return $this->getTag()->getChild('child')->getChild('group');
    }

    public function setTagId(string $id)
    {
        parent::setTagId($id);

        $this->getDefaultParent()->setId($id);

        $this->getTag()->getChild('label')->setAttribute('for', $id);
    }

    public function setTagLabel(string $label)
    {
        parent::setTagLabel($label);

        $this->getDefaultParent()->setLabel($label);

        $this->getTag()->getChild('label')->setChild(new Text($label), 'text');
    }
}
