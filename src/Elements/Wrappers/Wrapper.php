<?php

namespace MyHotelBike\LaravelForms\Elements\Wrappers;


use MyHotelBike\LaravelForms\Elements\Element;

abstract class Wrapper extends Element
{
    public static function wrap(Element $element) {
        $self = new static($element->getId() . '-wrapper', $element->getLabel());
        $self->addElement($element);

        return $self;
    }
}
