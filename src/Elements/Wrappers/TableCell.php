<?php

namespace MyHotelBike\LaravelForms\Elements\Wrappers;

use MyHotelBike\LaravelForms\Tags\EncapsulatingTag;
use MyHotelBike\LaravelForms\Tags\Tag;

class TableCell extends Wrapper
{
    public function buildTag(): Tag
    {
        $tag = new EncapsulatingTag('td');

        return $tag;
    }
}
