<?php

namespace MyHotelBike\LaravelForms\Elements\Containers;

use MyHotelBike\LaravelForms\Elements\Element;
use MyHotelBike\LaravelForms\Helpers\Text;
use MyHotelBike\LaravelForms\Tags\EncapsulatingTag;
use MyHotelBike\LaravelForms\Tags\Tag;

class Table extends Element
{
    public function buildTag(): Tag
    {
        $table = new EncapsulatingTag('table');
        $table->addAttribute('class', 'table');

        $caption = new EncapsulatingTag('caption');
        $table->setChild($caption, 'caption');

        $thead = new EncapsulatingTag('thead');
        $table->setChild($thead, 'thead');

        $tfoot = new EncapsulatingTag('tfoot');
        $table->setChild($tfoot, 'tfoot');

        $tbody = new EncapsulatingTag('tbody');
        $table->setChild($tbody, 'tbody');

        return $table;
    }

    public function setHeader(TableRow $row, string $id = 'header') {
        $this->getTag()->getChild('thead')->setChild($row, $id);
    }

    public function setFooter(TableRow $row, string $id = 'footer') {
        $this->getTag()->getChild('tfoot')->setChild($row, $id);
    }

    public function getDefaultParent()
    {
        return $this->getTag()->getChild('tbody');
    }

    public function setTagLabel(string $label)
    {
        $this->getTag()->getChild('caption')->setChild(new Text($label), 'text');
    }
}
