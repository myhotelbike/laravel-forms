<?php

namespace MyHotelBike\LaravelForms\Elements\Containers;


use MyHotelBike\LaravelForms\Elements\Element;
use MyHotelBike\LaravelForms\Tags\EncapsulatingTag;
use MyHotelBike\LaravelForms\Tags\Tag;

class Inline extends Element
{
    public function buildTag(): Tag
    {
        $tag = new EncapsulatingTag('div');
        $tag->addAttribute('class', 'form-inline');

        return $tag;
    }
}
