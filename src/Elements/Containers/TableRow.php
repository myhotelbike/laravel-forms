<?php

namespace MyHotelBike\LaravelForms\Elements\Containers;


use MyHotelBike\LaravelForms\Elements\Element;
use MyHotelBike\LaravelForms\Tags\EncapsulatingTag;
use MyHotelBike\LaravelForms\Tags\Tag;

class TableRow extends Element
{
    public function buildTag(): Tag
    {
        $row = new EncapsulatingTag('tr');

        return $row;
    }
}
