<?php

namespace MyHotelBike\LaravelForms\Elements\Containers;

use MyHotelBike\LaravelForms\Elements\Element;
use MyHotelBike\LaravelForms\Helpers\Text;
use MyHotelBike\LaravelForms\Tags\EncapsulatingTag;
use MyHotelBike\LaravelForms\Tags\Tag;

class Fieldset extends Element
{
    public function buildTag(): Tag
    {
        $tag = new EncapsulatingTag('fieldset');

        return $tag;
    }

    public function setTagLabel(string $label)
    {
        $legend = new EncapsulatingTag('legend');
        $legend->setChild(new Text($label), 'text');
        $this->setChild($legend, 'legend');
    }
}
