<?php

namespace MyHotelBike\LaravelForms\Elements;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Str;

use MyHotelBike\LaravelForms\Contracts\ElementContract;
use MyHotelBike\LaravelForms\Helpers\HasTraits;
use MyHotelBike\LaravelForms\Helpers\Renderable;
use MyHotelBike\LaravelForms\Tags\EncapsulatingTag;
use MyHotelBike\LaravelForms\Tags\SelfClosingTag;
use MyHotelBike\LaravelForms\Tags\Tag;

abstract class Element implements ElementContract, Renderable
{
    use HasTraits;

    /**
     * @var string
     */
    private $id;

    /** @var string */
    private $label;

    /** @var bool */
    private $isParent = false;

    /** @var array */
    private $rules = [];

    /** @var Tag */
    private $tag;

    public function __construct(string $id, string $label = '')
    {
        $this->bootIfNotBooted();

        $this->initializeTraits();

        $this->tag = $this->buildTag();

        $this->setId($id);

        $this->setLabel($label);
    }

    public abstract function buildTag();

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id)
    {
        $id = Str::slug($id, '_');
        $this->id = $id;

        $this->setTagId($id);
    }

    public function setTagId(string $id)
    {
        $this->setAttribute('id', $id);
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function setLabel(string $label)
    {
        $this->label = $label;

        $this->setTagLabel($label);
    }

    public function setTagLabel(string $label)
    {
        $this->setAttribute('title', $label);
    }

    public function getIsParent(): bool
    {
        return $this->isParent;
    }

    public function setIsParent(bool $is_parent = true)
    {
        $this->isParent = $is_parent;
    }

    public function addRule($rule, $id = null)
    {
        if (isset($id)) {
            $this->rules[$id] = $rule;
        } else {
            $this->rules[] = $rule;
        }

        return $this;
    }

    public function getRules(): array
    {
        $rules = [$this->id => array_values($this->rules)];

        foreach ($this->getChildren() as $child) {
            if (!($child instanceof self)) {
                continue;
            }

            foreach ($child->getRules() as $child_id => $child_rules) {
                $rules[$this->prefixChildId($child_id)] = $child_rules;
            }
        }

        return array_filter($rules);
    }

    public function getLabels(): array
    {
        $labels = [$this->id => $this->label];

        foreach ($this->getChildren() as $child) {
            if (!($child instanceof self)) {
                continue;
            }

            $labels[$this->prefixChildId($child->getId())] = $child->getLabel();
        }

        return array_filter($labels);
    }

    protected function prefixChildId($child_id)
    {
        if ($this->isParent) {
            $child_id = $this->id . '.' . $child_id;
        }

        return $child_id;
    }

    public function getTag()
    {
        return $this->tag;
    }

    public function setTag(Tag $tag)
    {
        $this->tag = $tag;
    }

    public function getValues(array $data): array
    {
        $values = [];

        if ($this->isParent) {
            $data = $data[$this->id] ?? [];
        } else {
            $values = array_merge_recursive($values, $this->getOwnValues($data));
        }

        $child_values = $this->getChildValues($data);

        if (empty($child_values)) {
            return $values;
        }

        if ($this->isParent) {
            $values[$this->id] = $child_values;
        } else {
            $values = array_merge($values, $child_values);
        }

        return $values;
    }

    public function getOwnValues($data): array {
        if (isset($data[$this->id])) {
            return [$this->id => $data[$this->id]];
        }

        return [];
    }

    protected function getChildValues(array $data): array
    {
        $child_values = [];

        foreach ($this->getChildren() as $child) {
            if ($child instanceof self) {
                $child_values = array_merge($child_values, $child->getValues($data));
            }
        }

        return $child_values;
    }

    public function setValues(array $data)
    {
        foreach ($this->getChildren() as $child) {
            if ($child instanceof self) {
                $child->setValues($this->isParent ? ($data[$this->getId()] ?? []) : $data);
            }
        }
    }

    public function render(array $parents = []): string
    {
        $html = $this->tag->renderOpening($parents);

        if ($this->isParent) {
            $parents[] = $this->id;
        }
        $html .= $this->tag->renderChildren($parents);

        $html .= $this->tag->renderClosing();

        return $html;
    }

    public function getDefaultParent()
    {
        return $this->tag;
    }

    public function addElement(Element $child)
    {
        $this->setChild($child, $child->getId());
    }

    public function setChild(Renderable $child, string $id)
    {
        $this->getDefaultParent()->setChild($child, $id);
    }

    public function getChild(string $id, $default = null)
    {
        return $this->getDefaultParent()->getChild($id, $default);
    }

    public function getChildren(): array
    {
        return $this->getDefaultParent()->getChildren();
    }

    public function removeChild(string $id)
    {
        $this->getDefaultParent()->removeChild($id);
    }

    public function clearChildren()
    {
        $this->getDefaultParent()->clearChildren();
    }

    public function setAttribute($key, $values)
    {
        $this->tag->setAttribute($key, $values);
    }

    public function getAttribute($key, $default = null)
    {
        return $this->tag->getAttribute($key, $default);
    }

    public function addAttribute($key, $values)
    {
        $this->tag->addAttribute($key, $values);
    }

    public function clearAttributes()
    {
        $this->tag->clearAttributes();
    }

    public function removeAttribute($key, $values = null)
    {
        $this->tag->removeAttribute($key, $values);
    }

    public function getAttributes()
    {
        return $this->tag->getAttributes();
    }

}
