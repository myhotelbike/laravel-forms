<?php

namespace MyHotelBike\LaravelForms\Elements\Buttons;

use MyHotelBike\LaravelForms\Tags\Tag;

class Submit extends Button
{
    public function buildTag(): Tag
    {
        $button = parent::buildTag();

        $button->setAttribute('type', 'submit');
        $button->addAttribute('class', ['btn-success']);
        $button->removeAttribute('class', 'btn-secondary');

        return $button;
    }
}
