<?php

namespace MyHotelBike\LaravelForms\Elements\Buttons;

use MyHotelBike\LaravelForms\Elements\Element;
use MyHotelBike\LaravelForms\Helpers\Text;
use MyHotelBike\LaravelForms\Tags\EncapsulatingTag;
use MyHotelBike\LaravelForms\Tags\Tag;

class Button extends Element
{
    public function buildTag(): Tag
    {
        $button = new EncapsulatingTag('button');
        $button->addAttribute('class', ['btn', 'btn-secondary']);
        $button->addAttribute('type', 'button');

        return $button;
    }

    public function setTagLabel(string $label)
    {
        parent::setTagLabel($label);

        $this->setChild(new Text($label), 'text');
    }
}
